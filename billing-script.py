import boto3
import json
from datetime import timedelta
import datetime
import ssl, smtplib
from pprint import pprint
import os

os.environ['AWS_PROFILE'] = "privecera-dev"
launchTime = None

def getlaunchTime():
    global launchTime
    return launchTime

def setlaunchTime(u1):
    global launchTime
    launchTime = u1

def send_message(SUBJECT, TEXT):
    
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    toaddr = 'irfan.hussain@gslab.com'
    cc = []
    fromaddr = 'irfan.hussain@gslab.com'
    message_subject = SUBJECT
    message_text = TEXT
    message = "From: %s\r\n" % fromaddr + "To: %s\r\n" % toaddr + "CC: %s\r\n" % ",".join(cc) + "Subject: %s\r\n" % message_subject + "\r\n"  + message_text
    toaddrs = [toaddr] + cc
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(fromaddr,password="nedzcwsymeqpelvx")
        server.sendmail(fromaddr, toaddrs, message)
        server.quit()

def get_running_instances():
    ec2_client = boto3.client("ec2", region_name="us-east-2")
    reservations = ec2_client.describe_instances(Filters=[
        {
            "Name": "instance-state-name",
            "Values": ["running"],
        },
        {
            "Name":"tag:team",
            "Values":["Testing"]
        }
    ]).get("Reservations")
    # print(type(reservations))
    pprint(reservations)

    for reservation in reservations:
        for instance in reservation["Instances"]:
            launchTime=instance['LaunchTime']
            # print('-------launch-time--------')
            # print(launchTime)
            setlaunchTime(launchTime)

    for reservation in reservations:
        for instance in reservation["Instances"]:
            instance_id = instance["InstanceId"]
            instance_type = instance["InstanceType"]
            public_ip = instance["PublicIpAddress"]
            private_ip = instance["PrivateIpAddress"]
            print(f"{instance_id}, {instance_type}, {public_ip}, {private_ip}")
    if reservations != []:
        return True     

res = get_running_instances()
if res:
    StartTime=getlaunchTime().strftime('%Y-%m-%dT%H:%M:%SZ')
    print('-------StartTime---------')
    print(StartTime)
    print('-------Actual-Endtime---------')
    print(datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'))
    time=datetime.datetime.utcnow() + timedelta(hours=1)
    EndTime=time.strftime('%Y-%m-%dT%H:%M:%SZ')
    print('-------modified-EndTime---------')
    print(EndTime)

    difference = datetime.datetime.utcnow().hour - getlaunchTime().hour  
    print('---------hour-difference-----------')
    print(difference)
    # if difference == 0:
    #     NewEndTime = getlaunchTime() + timedelta(hours=2)
    #     EndTime = NewEndTime.strftime('%Y-%m-%dT%H:%M:%SZ')
    #     print('-------New-EndTime-----')
    #     print(EndTime)

client = boto3.client('ce')

# result={
#     "GroupDefinitions": [
#         {
#             "Type": "DIMENSION",
#             "Key": "SERVICE"
#         },
#         {
#             "Type": "DIMENSION",
#             "Key": "USAGE_TYPE"
#         }
#     ],
#     "ResultsByTime": [
#         {
#             "TimePeriod": {
#                 "Start": "2021-05-28T08:00:00Z",
#                 "End": "2021-05-28T09:00:00Z"
#             },
#             "Total": {
#                 "BlendedCost": {
#                     "Amount": "7",
#                     "Unit": "USD"
#                 }
#             },
#             "Groups": [],
#             "Estimated": True
#         },
#         {
#             "TimePeriod": {
#                 "Start": "2021-05-28T09:00:00Z",
#                 "End": "2021-05-28T10:00:00Z"
#             },
#             "Total": {
#                 "BlendedCost": {
#                     "Amount": "3",
#                     "Unit": "USD"
#                 }
#             },
#             "Groups": [],
#             "Estimated": True
#         },
#         {
#             "TimePeriod": {
#                 "Start": "2021-05-28T10:00:00Z",
#                 "End": "2021-05-28T11:00:00Z"
#             },
#             "Total": {
#                 "BlendedCost": {
#                     "Amount": "7",
#                     "Unit": "USD"
#                 }
#             },
#             "Groups": [],
#             "Estimated": True
#         },
#         {
#             "TimePeriod": {
#                 "Start": "2021-05-28T11:00:00Z",
#                 "End": "2021-05-28T12:00:00Z"
#             },
#             "Total": {
#                 "BlendedCost": {
#                     "Amount": "6",
#                     "Unit": "USD"
#                 }
#             },
#             "Groups": [],
#             "Estimated": True
#         }
#     ],
#     "DimensionValueAttributes": [],
#     "ResponseMetadata": {
#         "RequestId": "b35c94ed-b277-4395-a294-ae09ed504eb7",
#         "HTTPStatusCode": 200,
#         "HTTPHeaders": {
#             "date": "Fri, 28 May 2021 11:25:29 GMT",
#             "content-type": "application/x-amz-json-1.1",
#             "content-length": "779",
#             "connection": "keep-alive",
#             "x-amzn-requestid": "b35c94ed-b277-4395-a294-ae09ed504eb7",
#             "cache-control": "no-cache"
#         },
#         "RetryAttempts": 0
#     }
# }




result = client.get_cost_and_usage(
    TimePeriod = {
        # 'Start': '2021-05-27',
        # 'End': '2021-05-28'
        'Start': StartTime,
        'End': EndTime
        # 'Start': '2021-05-27T11:11:11Z',
        # 'End': '2021-05-27T19:41:11Z'
    },
  

    Granularity = 'HOURLY',
    # Filter = {
    #     "And": [{
    #         "Dimensions": {
    #             "Key": "INSTANCE_TYPE",
    #             "Values": ["t2.micro"]
    #         }
    #     }, {
    #         "Not": {
    #             "Dimensions": {
    #                 "Key": "RECORD_TYPE",
    #                 "Values": ["Credit", "Refund"]
    #             }
    #         }
    #     }
    #    ]
    # },
    Metrics = ["BlendedCost"],
    GroupBy = [
        {
            'Type': 'DIMENSION',
            'Key': 'SERVICE'
        },
        {
            'Type': 'DIMENSION',
            'Key': 'USAGE_TYPE'
        }
    ]
)
totalcost = 0
for timeperiod in result['ResultsByTime']:
    print(timeperiod['Total']['BlendedCost']['Amount'])
    totalcost=totalcost + int(timeperiod['Total']['BlendedCost']['Amount'])
   
print('------Total-Cost------')
print(totalcost)

json_object = json.dumps(result, indent = 4)  
print(json_object) 
test = (f"this build cost ${totalcost}")
subject='jenkins billing pipeline'
send_message(subject, test)
